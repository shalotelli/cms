$(function() {
	/*****************
	 *  CONTROL BAR  *
	 *****************/ 

	// make control bar draggable
	$("#control-bar").draggable({
		// when dragging, hide toolbar
		start: function(event, ui) {
			$(this).find('#elements').hide();
		},

		// when dragging stops, show toolbar
		stop: function(event, ui) {
			$(this).find('#elements').show();
		}
	});

	// make toolbar draggable
	$("#control-bar > #elements > div").draggable({
		// on drag, clone element and add custom styles
		helper: function(event, ui) {
			return $(this).clone().css({
				background: '#46d1f7',
				color: '#fff',
				textShadow: 'none'
			});
		},

		// connect to container list
		connectToSortable: '.container'
	});

	// add .element-button class to each toolbar button
	$("#elements").children().each(function() {
		$(this).addClass('element-button');
	});

	// when close button click, remove element
	$(document).on('click', '.close', function() {
		console.log('test');
	});

	/************************
	 *  CONTAINER ELEMENTS  *
	 ************************/ 

	// functionality for every element
	$(".container > *")
	.each(function() {
		// reference element
		var el = $(this);

		// images need to be wrapped in div before adding close button
		if(el.is('img')) {
			el
			.wrap('<div />')
			.parent()
			.append('<div class="close">&times;</div>')
			.find(".close")
			.css({
				top: el.offset().top + 5,
				left: el.offset().left + el.outerWidth() - 20
			});
		} else {
			el
			.append('<div class="close">&times;</div>')
			.find(".close")
			.css({
				top: el.offset().top + 5,
				left: el.offset().left + el.outerWidth() - 20
			});
		}
	})

	// highlight element on hover
	.hover(function() {
		$(this)
		.addClass('moveable')
		.find(".close")
		.show();	
	}, function() {
		$(this)
		.removeClass('moveable')
		.find(".close")
		.hide();
	});

	/***********************
	 *  ELEMENT CONTAINER  *
	 ***********************/ 

	// make elements sortable
	$(".container").sortable({
		// when moving, hide close button and show placeholder
		start: function(event, ui) {
			$(".close").remove();

			if($(ui.item).is('img')) {
				$(ui.placeholder).attr('src', $(ui.item).attr('src'));
			}
		},

		// when stopped, process element and add
		stop: function(event, ui) {
			var el = $(ui.item),
				tag = el.attr('tag')

			// if new element
			if(el.hasClass('element-button')) {
				el
				// remove toolbar classes
				.removeClass('ui-draggable element-button')

				// add editable class
				.addClass('editable')

				// remove 'tag' attribute
				.removeAttr('tag')

				// update text
				.text('Click to edit');	

				// add appropriate tags
				switch(tag) {
					case 'img':
						el.wrap('<'+tag+' src="http://www.placehold.it/350x150" />');
					break;

					case 'a':
						el.wrap('<'+tag+' href="#" />');
					break;

					default:
						el.wrap('<'+tag+' />');
				}
			}
		},
		placeholder: "highlight-placeholder",
		revert: true
	});

	/***************************************
	 *  CONTROLS FOR EACH TYPE OF ELEMENT  *
	 ***************************************/ 

	$(document).on('click', 'img', function() {
		alert('image upload box');
	});

	$(document).on('click', '.container h1, .container h2, .container h3, .container p', function() {
		alert('text update box');
	});

	$(document).on('click', 'a', function() {
		alert('link update box');
	});

	/*********************
	 *  DISPLAY RESULTS  *
	 *********************/ 

	$("#get-html").on('click', function() {
		alert($('.container').html());
	});
});