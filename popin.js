(function($) {
	$.fn.popin = function(options) {
		var defaults = {
			width: 380,
			height: 280,
			holder: '',
			overlay: '',
			template: '<p>jQuery popin!</p>'
		};

		var options = $.extend(defaults, options);

		return this.each(function() {
			var $body = $('body'),
				maxWidth = options.width > 640 ? 640 : options.width,
				maxHeight = options.height > 350 ? 350 : options.height;

			$body.addClass('popin-read');
			$body.append('<div class="popin-overlay ' + options.overlay + '"></div>');				
			$body.append('<div class="popin ' + options.holder + '">' + options.template + '</div>');

			$('.popin').css({
				'width': maxWidth + 'px',
				'height': maxHeight + 'px',
				'margin-left': '-' + (maxWidth / 2 + 10) + 'px',
				'margin-top': '-' + (maxHeight / 2 + 10) + 'px'
			});

			var onDocumentClick = function(e) {
				if($(e.target).is('popin-overlay, .popin-close')) {
					deactivate();
				}
			}

			function activate() {
				$body
				.on('click', onDocumentClick)
				.addClass('popin-active');
			}

			function deactivate() {
				$body
				.off('click', onDocumentClick)
				.removeClass('popin-active');
			}

			$(this).on('click', function(e) {
				e.stopPropagation();
				activate();
			});
		});
	}
})(jQuery);